# Payment Service Provider Reporting API

## About

This API provides transaction and payment data for the Payment Service Provider (PSP) to use in reporting by merchants.

## Infrastructre

### Architecture

[Laravel Resources](https://laravel.com/docs/9.x/eloquent-resources) is used to manipulate and provide data through controllers.
### Authentication

As described in case documentation, the API requires an API Key. After the API Key is provided to the merchant, the merchant can use the API.

The app uses an implementation of [JWT](https://jwt.io/) for Laravel to authenticate users. When the user logins to the app with valid credentials, the app returns a signed access token back to the user, which is valid for a limited time period to use in further requests.  When the token expires, the app returns a new access token to the user in the Authorization header of the response with very next request. So, client can check if the response has a new access token and keep user signed in.

### Third parties

- Libraries
  - [tymon/jwt-auth](https://github.com/tymondesigns/jwt-auth)

## Installation

- Clone the repository
- Install dependencies via composer
  ```bash
  $ composer install
  ```
- Generate JWT secret via following command
  ```bash 
  $ php artisan jwt:secret
  ```
- Run docker container via command below
  ```bash
  $ vendor/sail up -d #-d for detached mode
  ```
- Run migrations
  ```bash
  $ vendor/sail artisan migrate
  ```
- Run seeds
  ```bash
  $ vendor/sail artisan db:seed

## Postman Collection

In order to test the API, you can use [Postman Collection](https://www.postman.com/collections/335942101a2028845532).
