<?php

namespace App\Models;

use App\Models\Scopes\MerchantScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    use HasFactory;

    protected $primaryKey = 'transaction_id';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new MerchantScope);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public function acquirer()
    {
        return $this->belongsTo(Acquirer::class, 'acquirer_id', 'id');
    }

    public function foreignExchangeTransaction()
    {
        return $this->hasOne(ForeignExchangeTransaction::class, 'id', 'foreign_exchange_transaction_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function ipn()
    {
        return $this->hasOne(InstantPaymentNotification::class, 'transaction_id', 'transaction_id');
    }

    public function getIsIpnReceivedAttribute()
    {
        return DB::table('instant_payment_notifications')
            ->where('transaction_id', $this->transaction_id)
            ->exists();
    }
}
