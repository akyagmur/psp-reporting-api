<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->get('apiToken');

        if (!$token) {
            // If the token is not provided and there is a user logged in, kill the session
            if(Auth::check()) {
                Auth::logout();
            }

            return response()->json([
                'code'   => Response::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized'
            ]);
        }

        return $next($request);
    }
}
