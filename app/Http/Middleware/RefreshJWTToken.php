<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class RefreshJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json([
                    'code'   => Response::HTTP_UNAUTHORIZED,
                    'message' => 'Unauthorized'
                ]);
            }
        } catch (TokenExpiredException $e) {
            // If the token is expired, then it will be refreshed and added to the headers
            try {
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed)->toUser();
                header('Authorization: Bearer ' . $refreshed);
            } catch (JWTException $e) {
                return response()->json([
                    'code'   => Response::HTTP_UNAUTHORIZED,
                    'message' => 'Unauthorized'
                ]);
            }
        } catch (JWTException $e) {
            return response()->json([
                'code'   => Response::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized'
            ]);
        }

        if (!Auth::check()) {
            $this->authenticateUser($user, $request);
        }

        return  $next($request);
    }

    /**
     * Authenticate the user.
     *
     * @param  \App\Models\User  $user
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    private function authenticateUser(User $user, Request $request)
    {
        $apiToken = $request->get('apiToken');

        // If provided api token belongs to the user, then we'll allow user to continue
        $token = $user->tokens()->where('token', hash('sha256', $apiToken))->first();

        if ($token) {
            // Update the token's last used date
            $token->update([
                'last_used_at' => now()
            ]);

            return Auth::login($user, false);
        }

        return response()->json([
            'code'   => Response::HTTP_UNAUTHORIZED,
            'message' => 'Unauthorized'
        ]);
    }
}
