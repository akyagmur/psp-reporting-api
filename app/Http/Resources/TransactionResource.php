<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'fx' => new FXResource($this->foreignExchangeTransaction),
            'customer' => new CustomerResource($this->customer),
            'merchant' => [
                'name' => $this->merchant->name,
            ],
            'transaction' => [
                'transaction_id' => $this->transaction_id,
                'reference_number' => $this->reference_number,
                'merchant_id' => $this->merchant_id,
                'status' => $this->status,
                'channel' => $this->channel,
                'operation' => $this->operation,
                'fx_transaction_id' => $this->foreign_exchange_transaction_id,
                'updated_at' => $this->updated_at,
                'created_at' => $this->created_at,
            ]
        ];
    }
}
