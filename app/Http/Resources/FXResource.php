<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FXResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'original_amount' => $this->original_amount,
            'original_currency' => $this->original_currency
        ];
    }
}
