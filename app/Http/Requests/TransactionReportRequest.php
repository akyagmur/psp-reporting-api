<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'fromDate' => 'required|date|before:toDate|date_format:Y-m-d',
            'toDate' => 'required|date|after:fromDate|date_format:Y-m-d',
            'merchantId' => 'sometimes|exists:merchants,id|integer'
        ];
    }
}
