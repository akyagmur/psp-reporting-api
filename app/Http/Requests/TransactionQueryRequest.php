<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionQueryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'fromDate' => 'required|date|before:toDate|date_format:Y-m-d',
            'toDate' => 'required|date|after:fromDate|date_format:Y-m-d',
            'merchantId' => 'sometimes|exists:merchants,id|integer',
            'status' => 'sometimes|in_array:APPROVED,WAITING,DECLINED,ERROR',
            'operation' => 'sometimes|in_array:DIRECT,REFUND,3D,3DAUTH,STORED',
            'channel' => 'sometimes|in_array:WEB,MOBILE,POS',
            'paymentMethod' => 'sometimes|in_array:CREDITCARD',
            'errorCode' => 'sometimes|in_array:Do not honor, Invalid Transaction, Invalid Card, Not sufficient funds, Incorrect PIN, Invalid country association, Currency not allowed, 3-D Secure Transport Error, Transaction not permitted to cardholder',
            'filterField' => 'sometimes|required_with:filterValue|in_array:Transaction UUID, Customer Email, Reference No, Custom Data, Card PAN',
            'filterValue' => 'sometimes|required_with:filterField|string|max:256',
            'page' => 'integer'
        ];
    }
}
