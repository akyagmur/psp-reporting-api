<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionQueryRequest;
use App\Http\Requests\TransactionReportRequest;
use App\Http\Requests\TransactionRequest;
use App\Http\Resources\TransactionCollection;
use App\Http\Resources\TransactionResource;
use App\Models\Transaction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// TODO: Make a Transaction Repository
class TransactionController extends Controller
{
    private const PAGINAL_LIMIT = 10;

    /**
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransaction(TransactionRequest $request): JsonResponse
    {
        $transaction = Transaction::findOrFail($request->transactionId);

        return response()->json([
            'status' => 'APPROVED',
            'response' => new TransactionResource($transaction)
        ]);
    }

    /**
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionQuery(TransactionQueryRequest $request): JsonResponse
    {
        $transactions = Transaction::whereBetween('created_at', [
            $request->fromDate,
            $request->toDate,
        ])
            ->when($request->merchantId, function ($query) use ($request) {
                return $query->where('merchant_id', $request->merchantId);
            })
            ->when($request->status, function ($query) use ($request) {
                return $query->where('status', $request->status);
            })
            ->when($request->operation, function ($query) use ($request) {
                return $query->where('operation', $request->operation);
            })
            ->when($request->channel, function ($query) use ($request) {
                return $query->where('channel', $request->channel);
            })
            ->when($request->paymentMethod, function ($query) use ($request) {
                return $query->where('payment_method', $request->paymentMethod);
            })
            ->when($request->errorCode, function ($query) use ($request) {
                return $query->where('error_code', $request->errorCode);
            })
            ->when($request->filterField && $request->filterValue, function ($query) use ($request) {
                return $query->where($request->filterField, $request->filterValue);
            })
            ->paginate(self::PAGINAL_LIMIT);

        return response()->json([
            'status' => 'APPROVED',
            'response' => new TransactionCollection($transactions)
        ]);
    }

    /**
     * @param TransactionReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionReport(TransactionReportRequest $request): JsonResponse
    {
        $report = $this->makeTransactionReport($request);

        return response()->json([
            'status' => 'APPROVED',
            'response' => $report,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    private function makeTransactionReport(Request $request)
    {
        return DB::table('transactions')
            ->whereBetween('created_at', [
                $request->fromDate,
                $request->toDate,
            ])
            ->when($request->merchantId, function ($query) use ($request) {
                return $query->where('merchant_id', $request->merchantId);
            })
            ->select(
                DB::raw('
                    count(*) as quantity,
                    sum(foreign_exchange_transactions.original_amount) as total,
                    foreign_exchange_transactions.original_currency
                ')
            )
            ->join(
                'foreign_exchange_transactions',
                'transactions.foreign_exchange_transaction_id',
                '=',
                'foreign_exchange_transactions.id'
            )
            ->groupBy('foreign_exchange_transactions.original_currency')
            ->get();
    }

    public function getClient(Request $request): JsonResponse
    {
        $transaction = Transaction::find($request->transactionId);

        return response()->json([
            'status' => 'APPROVED',
            'response' => $transaction->client,
        ]);
    }
}
