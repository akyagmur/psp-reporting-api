<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['api', 'check-api-token'],
    'prefix' => 'v3'
], function ($router) {
    Route::post('/merchant/user/login',         [AuthController::class, 'login'])->name('merchant.user.login');
    Route::post('/merchant/user/register',      [AuthController::class, 'register'])->name('merchant.user.register');
    Route::post('/merchant/user/refresh-token', [AuthController::class, 'refreshToken'])->name('merchant.user.refresh_token');
    Route::post('/merchant/user/logout',        [AuthController::class, 'logout'])->name('merchant.user.logout');

    Route::group([
        'middleware' => ['auth:api', 'refresh-jwt']
    ], function ($router) {
        Route::get('/merchant/user/profile',        [AuthController::class, 'getProfile'])->name('merchant.user.get_profile');


        Route::get('/transactions/report',                 [TransactionController::class, 'getReport'])->name('transactions.report');
        Route::get('/transactions/get-transaction',        [TransactionController::class, 'getTransaction'])->name('transactions.get_transaction');
        Route::get('/transactions/get-transaction-report', [TransactionController::class, 'getTransactions'])->name('transactions.get_transactions');
        Route::get('/transactions/get-transaction-query',  [TransactionController::class, 'getTransactionQuery'])->name('transactions.get_transaction_query');
        Route::get('/transactions/get-client',             [TransactionController::class, 'getClient'])->name('transactions.get_client');
    });
});
