<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ForeignExchangeTransaction>
 */
class ForeignExchangeTransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'original_amount' => $this->faker->randomFloat(2, 0, 100),
            'original_currency' => $this->faker->randomElement(['USD', 'EUR', 'GBP', 'JPY', 'CNY', 'AUD', 'CAD', 'CHF', 'HKD', 'NZD', 'SGD', 'THB', 'ZAR']),
        ];
    }
}
