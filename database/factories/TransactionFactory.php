<?php

namespace Database\Factories;

use App\Models\Acquirer;
use App\Models\Customer;
use App\Models\ForeignExchangeTransaction;
use App\Models\Merchant;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $status = $this->faker->randomElement(['APPROVED', 'WAITING', 'DECLINED', 'ERROR']);
        return [
            'transaction_id' => $this->faker->uuid,
            'merchant_id' => 1,
            'foreign_exchange_transaction_id' => ForeignExchangeTransaction::factory(),
            'customer_id' => Customer::factory(),
            'acquirer_id' => rand(1, 10),
            'reference_number' => $this->faker->uuid,
            'status' => $status,
            'channel' => $this->faker->randomElement(['WEB', 'MOBILE', 'POS']),
            'operation' => $this->faker->randomElement(['DIRECT', 'REFUND', '3D', '3DAUTH', 'STORED']),
            'error_code' => $status == 'ERROR' ? $this->faker->randomElement(["Do not honor", "Invalid Transaction", "Invalid Card", "Not sufficient funds", "Incorrect PIN", "Invalid country association", "Currency not allowed", "3-D Secure Transport Error", "Transaction not permitted to cardholder"]) : null,
        ];
    }
}
