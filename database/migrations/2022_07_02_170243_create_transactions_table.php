<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('transaction_id')->primary();
            $table->unsignedBigInteger('merchant_id');
            $table->unsignedBigInteger('foreign_exchange_transaction_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('acquirer_id');
            $table->uuid('reference_number');
            $table->string('status');
            $table->string('channel');
            $table->string('operation');
            $table->string('error_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
