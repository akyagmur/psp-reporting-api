<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /* "number"
"expiryMonth"
"expiryYear"
"startMonth"
"startYear"
"issueNumber"
"email"
"birthday"
"gender"
"billingTitle"
"billingFirstName"
"billingLastName"
"billingCompany"
"billingAddress1"
"billingAddress2"
"billingCity"
"billingPostcode"
"billingState"
"billingCountry"
"billingPhone"
"billingFax"
"shippingTitle"
"shippingFirstName"
"shippingLastName"
"shippingCompany"
"shippingAddress1"
"shippingAddress2"
"shippingCity"
"shippingPostcode"
"shippingState"
"shippingCountry"
"shippingPhone"
"shippingFax" */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->date('birthday');
            $table->string('gender');
            $table->longText('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
