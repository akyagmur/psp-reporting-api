<?php

namespace Database\Seeders;

use App\Models\Acquirer;
use App\Models\Customer;
use App\Models\ForeignExchangeTransaction;
use App\Models\InstantPaymentNotification;
use App\Models\Merchant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Acquirer::factory()
            ->count(10)
            ->create();

        \App\Models\Merchant::factory()
            ->count(1)
            ->create();

        \App\Models\Transaction::factory()
            ->count(3)
            ->create()
            ->each(function ($transaction) {
                $transaction->ipn()->save(InstantPaymentNotification::factory()->make([
                    'transaction_id' => $transaction->transaction_id,
                ]));
            });

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
